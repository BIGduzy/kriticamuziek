<?php
/**
* 
*/
class Userscontroller{
	

	private $users;
	
	function __construct($users){
		$this->users = $users;

		for ($i = 0; $i < count($users); $i++) {
			$user = new user($users[$i]['User']); 
		}
	}

	/**
	*True = email exists
	*/
	public function checkEmail($email){
		for ($i=0; $i < count($this->users); $i++) { 
			if($this->users[$i]['User']['email'] == $email ){
				return true;
			}
		}
		return false;
	}

	/**
	*False = Niet geactiveerd
	*1 = activated
	*/
	public function checkActivated($email){	
		for ($i=0; $i < count($this->users); $i++) { 

			if($this->users[$i]['User']['email'] == $email ){

				if($this->users[$i]['User']['activated'] == 1){
					return true;
				}
			}
		}
		return false;
	}

	/**
	* True = user exists
	*/
	public function checkUser($email,$password){
		for ($i=0; $i < count($this->users); $i++) { 
			if($this->users[$i]['User']['email'] == $email && $this->users[$i]['User']['password'] == $password){
				return true;
			}
		}
		return false;
	}

	public function getUserByEmail($email){
		for ($i=0; $i < count($this->users); $i++) { 
			if($this->users[$i]['User']['email'] == $email){
				return $this->users[$i]['User'];
			}
		}
	}
}
?>