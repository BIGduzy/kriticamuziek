<?php
class BandController{

	private $band;
	private $bandleden;

	public function __construct($bandArray,$bandledenArray){
		$this->band = $bandArray;
		$this->bandleden = $bandledenArray;

		for ($i = 0; $i < count($this->band); $i++) {
			$this->band[$i] = new Band($this->band[$i]['Band']);
		}
	}

	public function getBandID($i){
		return $this->band[$i]->getBandID();
	}

	public function getName($id){
		for ($i=0; $i < count($this->band); $i++) { 
			$bandID = $this->band[$i]->getBandID();

			if($bandID == $id){
				return $this->band[$i]->getName();
			}
		}
	}

	public function getBandLeden($bandID,$artists){
		$leden = array();

		for ($i=0; $i < count($this->bandleden); $i++) { 

			if ($bandID == $this->bandleden[$i]['Bandleden']['band']) {

				for ($j=0; $j < count($artists); $j++) { 
					
					if($artists[$j]['Artist']['artistID'] == $this->bandleden[$i]['Bandleden']['artist']){

						$name = $artists[$j]['Artist']['name'];
						array_push($leden, $name);
					}
				}
			}
		}

		return $leden;
	}

	public function getBandLedenID($bandID,$artists){
		$leden = array();

		for ($i=0; $i < count($this->bandleden); $i++) { 

			if ($bandID == $this->bandleden[$i]['Bandleden']['band']) {

				for ($j=0; $j < count($artists); $j++) { 
					
					if($artists[$j]['Artist']['artistID'] == $this->bandleden[$i]['Bandleden']['artist']){

						$id = $artists[$j]['Artist']['artistID'];
						array_push($leden, $id);
					}
				}
			}
		}

		return $leden;
	}
}
?>