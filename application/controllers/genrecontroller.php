<?php
class GenreController{
	
	private $genre;

	public function __construct($genreArray){
		$this->genre = $genreArray;

		for ($i = 0; $i < count($genreArray); $i++) { 
			$this->genre[$i] = new Genre($this->genre[$i]['Genre']);
		}

	}

	public function getName($id){
		for ($i=0; $i < count($this->genre); $i++) { 
			$genreID = $this->genre[$i]->getGenreID();

			if($genreID == $id){
				return $this->genre[$i]->getName();
			}
		}
	}

	public function getGenreID($id){
		for ($i=0; $i < count($this->genre); $i++) { 
			$genreID = $this->genre[$i]->getGenreID();

			if($genreID == $id){
				return $this->genre[$i]->getGenreID();
			}
		}
	}
}
?>