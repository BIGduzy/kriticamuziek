<?php
	class GebruikersController extends controller{

		public function homepage($type = "tracks"){	
			if(!$this->checkLogin()){
				header("Location:".BASE_URL."bezoekers/homepage");
				echo "Niet ingelogd";
				exit();
			}

			$this->set("filter","tracks");
			$this->set("title","Mijn muziek");

			switch ($type) {
				case 'albums':
					$this->showAlbums();
					break;
				case 'tracks':
					$this->showSongs();
					break;
				case 'artiesten':
					$this->showArtists();
					break;
				case 'band':
					$this->showBands();
					break;
				case 'genre':
					$this->showGenre();
					break;	
				
				default:
					$this->showSongs();
					break;
			}
		}

		public function add(){
			if(!$this->checkLogin()){
				header("Location:".BASE_URL."bezoekers/homepage");
				echo "Niet ingelogd";
				exit();
			}

			//set logo
			$logo = BASE_URL."public/img/kriticalogo.png";
			$this->set("logo",$logo);

			$message = "";

			$this->addSong();
		}

		public function update($type="",$song="",$amount=0){
			if(!$this->checkLogin()){
				header("Location:".BASE_URL."bezoekers/homepage");
				echo "Niet ingelogd";
				exit();
			}

			$logo = BASE_URL."public/img/kriticalogo.png";
			$this->set("logo",$logo);
			$this->set("songID",$song);
			$this->set("amount",$amount);
			$message = "";
			$showForm;


			$music = $this->_model->getMusicByMusicID($song);

			//if music is not set
			if (isset($music[0]['Music'])) {
				$artist = $this->_model->getArtists();
				$band = $this->_model->getBands();
				$bandleden = $this->_model->getBandleden();
				$genre = $this->_model->getGenres();
				$albums = $this->_model->getAlbums();
				$musiccontroller = new MusicController($music,$artist,$band,$bandleden,$genre,$albums);

				$music = $music[0]['Music'];

				$musicID = $music['musicID'];
				$songName = $music['name'];
				$description = $music['description'];
				$typeSong = $music['typeSong'];
				$typeIMG = $music['typeIMG'];
				$genre = $musiccontroller->getGenre(0);	
				$genreID = $music['genre'];			
				$songName = $musiccontroller->getName(0);
				$songNameSrc =  str_replace(' ', '', $songName);
				$location = $music['location'];
				$this->set("location",$location);


				$typeSongForDB = $music['typeSong'];
				$typeIMGForDB = $music['typeIMG'];


				//If artist
				if($type == "artist"){
					//get artist
					$artist = $musiccontroller->getArtist(0);
					$artistID = $music['artist'];
					
					
					if ($typeIMG != "") {
						$imgSrc = BASE_URL."public/img/".$songNameSrc."-".str_replace(' ', '', $artist).".".$typeIMG;
					}
					else{
						$imgSrc = BASE_URL."public/img/kriticalogo.png";
					}
					$IMG = "<img  class='img-thumbnail' src='".$imgSrc." '>";
					$this->set("IMG",$IMG);

					if ($typeSong != ""){
						$songSrc = BASE_URL."public/song/".$songNameSrc."-".str_replace(' ', '', $artist).".".$typeSong;

						//sets typeSong for the audio player
						switch ($typeSong) {
							case 'mp3':
								$typeSong = "audio/mpeg";
								break;
							case 'Ogg':
								$typeSong = "audio/ogg";
								break;
							case 'Wav':
								$typeSong = "audio/wav";
								break;
						}

						$song = "
							<audio preload='auto' controls='controls'>
								<source src='".$songSrc."' type='".$typeSong."'>
								Your browser does not support the audio element.
							</audio>";
						
					}else{
						$song = "<p> Geen song geupload </p>";
					}
					$this->set("song",$song);

					$artistInput = "
										<!-- Artist -->
										<div class='form-group'>
											<label for='inputArtist' class='col-sm-2 control-label'>Artiest:</label>
											<div class='col-sm-8'>
												<input type='text' class='form-control' id='inputArtist'
														name='inputArtist' value='".$artist."'>
											</div>
										</div>";
					$this->set("artistInput",$artistInput);
					$showForm = 1;

					//Submit
					if(isset($_POST['submit'])){
						$genre = array( 'genreID' => $genreID,
										'name' => $_POST['inputGenre'],
										'description' => '');

						$artist = array( 'artistID' => $artistID,
										'name' => $_POST['inputArtist'],
										'bio' => '');

						$music = array( 'musicID' => $musicID,
										'name' => $_POST['inputName'],
										'description' => $_POST['inputDescription'],
										'typeSong' => $typeSongForDB,
										'typeIMG' => $typeIMG,
										'location' => $_POST['inputLocation'],
										'genre' => $genre,
										'artist' => $artist);

						$this->_model->updateMusicArtist($music);

						// upload song
						if ($_FILES['inputSong']['size'] != 0) {
							$this->uploadSong(str_replace(' ', '',$music['name']) ,
												str_replace(' ', '',$music['artist']['name']));
						}
						// upload IMG
						if ($_FILES['inputIMG']['size'] != 0) {
							$this->uploadIMG(str_replace(' ', '',$music['name']) ,
											str_replace(' ', '',$music['artist']['name']));
						}
							
						$songName = "";
						$description = "";
						$genre = "";
						$message = "<p class='bg-success'> Het nummer is gewijzigd </p>";
						$showForm = 0;
					}//isset submit



				}//if artist
				else if($type == "band"){
					$band = $musiccontroller->getBand(0);
					$bandID = $music['band'];

					// img and song display
					if ($typeIMG != "") {
						$imgSrc = BASE_URL."public/img/".$songNameSrc."-".str_replace(' ', '', $band).".".$typeIMG;
					}
					else{
						$imgSrc = BASE_URL."public/img/kriticalogo.png";
					}
					$IMG = "<img  class='img-thumbnail' src='".$imgSrc." '>";
					$this->set("IMG",$IMG);

					if ($typeSong != ""){
						$songSrc = BASE_URL."public/song/".$songNameSrc."-".str_replace(' ', '', $band).".".$typeSong;

						//sets typeSong for the audio player
						switch ($typeSong) {
							case 'mp3':
								$typeSong = "audio/mpeg";
								break;
							case 'Ogg':
								$typeSong = "audio/ogg";
								break;
							case 'Wav':
								$typeSong = "audio/wav";
								break;
						}

						$song = "
							<audio preload='auto' controls='controls'>
								<source src='".$songSrc."' type='".$typeSong."'>
								Your browser does not support the audio element.
							</audio>";
						
					}else{
						$song = "<p> Geen song geupload </p>";
					}
					$this->set("song",$song);

					//if amount is a number > 0
					if(is_numeric($amount) && $amount > 0){
						$message =  "<p class='bg-success'> Band </p>";

						$bandInput = "
								<!-- Band -->
								<div class='form-group'>
									<label for='inputBand' class='col-sm-2 control-label'>Band:</label>
									<div class='col-sm-8'>
										<input type='text' class='form-control' id='inputBand'
											 name='inputBand' value='".$band."'>
									</div>
								</div>";

						$bandleden = $musiccontroller->getBandleden(0);
						for ($i=0; $i < count($bandleden); $i++) { 
							//get artist
							$artist = $bandleden[$i];
							$nummer = $i+1;

							$bandInput .= "
								<!-- Artist -->
								<div class='form-group'>
									<label for='inputArtist".$nummer."' class='col-sm-2 control-label'>Artiest ".$nummer.":</label>
									<div class='col-sm-8'>
										<input type='text' class='form-control' id='inputArtist".$i."'
													 name='inputArtist".$i."' value='".$artist."'>
									</div>
								</div>";
						}
						$this->set("bandInput",$bandInput);
						$showForm = 1;
					}
					else{ //if $_POST['inputAmount'] is not a posative number
						$message = "<p class='bg-info'> Het aantal artiesten meer dan 0 zijn. </p>";
						$showForm = 0;
					}


					//Submit
					if(isset($_POST['submit'])){

						// if set $_files song
						if ($_FILES['inputSong']['size'] != 0) {
							$typeSongForDB = $_FILES['inputSong']['name'];
							$typeSongForDB = explode('.', $typeSongForDB);
							$typeSongForDB = $typeSongForDB[1];
						}
						// if set $_files img
						if ($_FILES['inputIMG']['size'] != 0) {
							$typeIMGForDB = $_FILES['inputIMG']['name'];
							$typeIMGForDB = explode('.', $typeIMGForDB);
							$typeIMGForDB = $typeIMGForDB[1];
						}

						$genre = array( 'genreID' => $genreID,
										'name' => $_POST['inputGenre'],
										'description' => '');

						$band = array( 	'bandID' => $bandID,
										'name' => $_POST['inputBand'],
										'bio' => '');

						//puts all artist in array
						$bandleden = $musiccontroller->getBandledenID(0);
						for ($i=0; $i < $amount; $i++) { 
							$inputArtist = "inputArtist".$i;
							$musicID = $bandleden[$i];
							$artists[$i]['artistID'] = $musicID;
							$artists[$i]['name'] = $_POST[$inputArtist];
						}

						$music = array( 'musicID' => $music['musicID'],
										'name' => $_POST['inputName'],
										'description' => $_POST['inputDescription'],
										'typeSong' => $typeSongForDB,
										'typeIMG' => $typeIMGForDB,
										'location' => $_POST['inputLocation'],
										'genre' => $genre,
										'band' => $band,
										'artists' => $artists);
						$this->_model->updateMusicBand($music);

						// upload song
						if ($_FILES['inputSong']['size'] != 0) {
							$this->uploadSong(str_replace(' ', '',$music['name']) ,
												str_replace(' ', '',$music['band']['name']));
						}
						// upload IMG
						if ($_FILES['inputIMG']['size'] != 0) {
							$this->uploadIMG(str_replace(' ', '',$music['name']) ,
											str_replace(' ', '',$music['band']['name']));
						}
							
						$songName = "";
						$description = "";
						$genre = "";
						$message = "<p class='bg-success'> Het nummer is gewijzigd </p>";
						$showForm = 0;
					}//isset submit



				}// $type == "band"
				else{
					$message = "<p class='bg-info'> Ga naar de homepagina en klik op een song om deze te wijzigen. </p>";
					$showForm = 0;
				}
			}// music = null
			else{
				$songName = "";
				$description = "";
				$genre = "";
				$message = "<p class='bg-info'> Ga naar de homepagina en klik op een song om deze te wijzigen. </p>";
				$showForm = 0;
			}

			$this->set("type",$type);
			$this->set("showForm",$showForm);
			$this->set("songName",$songName);
			$this->set("description",$description);
			$this->set("genre",$genre);
			$this->set('message',$message);
		}

		public function removeMusic($type, $song=""){
			if(!$this->checkLogin()){
				header("Location:".BASE_URL."bezoekers/homepage");
				echo "Niet ingelogd";
				exit();
			}

			$music = $this->_model->getMusicByMusicID($song);			
			$artist = $this->_model->getArtists();
			$band = $this->_model->getBands();
			$bandleden = $this->_model->getBandleden();
			$genre = $this->_model->getGenres();
			$musiccontroller = new MusicController($music,$artist,$band,$bandleden,$genre);
			$genre = $musiccontroller->getGenre(0);
			$artist = $musiccontroller->getArtist(0);
			$band = $musiccontroller->getBand(0);
			$music = $music[0]['Music'];			
			$genreID = $music['genre'];	
			$artistID = $music['artist'];	
			$bandID = $music['band'];		

			$genre = array( 'genreID' => $genreID,
								'name' => $genre,
								'description' => '');

			if($type == 'artist')
			{
				$artist = array( 'artistID' => $artistID,
								'name' => $artist,
								'bio' => '');

				$music = array( 'musicID' => $music['musicID'],
								'name' => $music['name'],
								'description' => $music['description'],
								'typeSong' => $music['typeSong'],
								'typeIMG' => $music['typeIMG'],
								'location' => $music['location'],
								'genre' => $genre,
								'artist' => $artist);

				$this->_model->removeMusicArtist($music,$_SESSION['userID']);
			}
			else if($type == 'band'){
				$band = array( 'bandID' => $bandID,
								'name' => $band,
								'bio' => '');

				$bandleden = $musiccontroller->getBandledenID(0);
				for ($i=0; $i < count($bandleden); $i++) { 
					$musicID = $bandleden[$i];
					$artists[$i]['artistID'] = $musicID;
				}

				$music = array( 'musicID' => $music['musicID'],
								'name' => $music['name'],
								'description' => $music['description'],
								'typeSong' => $music['typeSong'],
								'typeIMG' => $music['typeIMG'],
								'location' => $music['location'],
								'genre' => $genre,
								'band' => $band,
								'artists' => $artists);
				$this->_model->removeMusicBand($music,$_SESSION['userID']);
			}

			header("Location:".BASE_URL."gebruikers/homepage");
		}

		public function logout(){

			if ( isset($_SESSION['userID'])) {
				session_destroy();
				header("Location:".BASE_URL."bezoekers/homepage");
			}
		}

		public function checkLogin(){
			if ( isset($_SESSION['userID'])) {
				return true;
			}
			else{
				return false;
			}
		}

		public function genre($genreID){

			$this->showSongs($genreID);
			$this->set("filter","genre");
			$this->set("title","Genres");

		}

		public function showAlbum($albumID){

			$this->showSongs(null,$albumID);
			$this->set("filter","albums");
		}

		public function showAlbums(){
			if(isset($_POST['searchQuery']) && isset($_POST['inputSearch']) ){
				$albums = $this->_model->searchAlbums($_POST['inputSearch']);
			}
			else{
				$albums = $this->_model->getAlbums();
			}

			$songs = "";
			for ($i=0; $i < count($albums); $i++) { 
				$albumID = $albums[$i]['Album']['albumID'];
				$albumName = $albums[$i]['Album']['name'];
				$albumDesc = $albums[$i]['Album']['description'];

				if($albumDesc == NULL){
					$albumDesc = "Geen omschrijving beschikbaar";
				}

				$songs .= "
						<div class='list-group'>
							<a href='".BASE_URL."gebruikers/showAlbum/".$albumID."' class='col-md-12 list-group-item '>	
								<div class='col-md-10'>
									<div class='row'>
										<p class='text-left'> Naam: ".$albumName."</p>
									</div>
									<div class='row'>
										<p class='text-left'> Omschrijving: ".$albumDesc."</p>
									</div> 
								</div>
							</a>
						</div>";
			}

			$this->set("filter","albums");
			$this->set("title","Albums");
			$this->set("songs",$songs);

		}

		public function showGenre(){
			if(isset($_POST['searchQuery']) && isset($_POST['inputSearch']) ){
				$genres = $this->_model->searchGenres($_POST['inputSearch']);
			}
			else{
				$genres = $this->_model->getGenres();
			}

			$songs = "";

			for($i = 0; $i < count($genres); $i++){
				$genreID = $genres[$i]['Genre']['genreID'];
				$genreName = $genres[$i]['Genre']['name'];
				$genreDesc = $genres[$i]['Genre']['description'];

				if($genreDesc == NULL){
					$genreDesc = "Geen omschrijving beschikbaar";
				}

				$songs .= "
						<div class='list-group'>
							<a href='".BASE_URL."gebruikers/showGenre/".$genreID."' class='col-md-12 list-group-item '>	
								<div class='col-md-10'>
									<div class='row'>
										<p class='text-left'> Naam: ".$genreName."</p>
									</div>
									<div class='row'>
										<p class='text-left'> Omschrijving: ".$genreDesc."</p>
									</div> 
								</div>
							</a>
						</div>";
			}

			$this->set("filter","genre");
			$this->set("title","Genres");
			$this->set("songs",$songs);

		}

		public function showBands(){
			if (isset($_POST['searchQuery']) ){
				$bands = $this->_model->searchBands($_POST['inputSearch']);
			}
			else{
				$bands = $this->_model->getBands();
			}	
			$bandleden = $this->_model->getBandleden();
			$artists = $this->_model->getArtists();

			$message = "";
			if (count($bands) <=0) {
				$message = "<p class='bg-success'> Er zijn nog geen bands toegevoegd </b>";
			}

			$bandController = new BandController($bands, $bandleden);

			$songs = "";
			for ($i=0; $i < count($bands); $i++) { 
				$bandName = $bands[$i]['Band']['name'];
				$bandBio = $bands[$i]['Band']['bio'];
				$bandleden = $bandController->getBandleden($bands[$i]['Band']['bandID'],$artists);

				if($bandBio == NULL){
					$bandBio = "Geen omschrijving beschikbaar";
				}

				//all artist in the band
				$leden = "";
				for ($j=0; $j < count($bandleden); $j++) { 
					if($j == count($bandleden)-1){ //if is last
						$leden .= $bandleden[$j]."";
					}
					else{
						$leden .= $bandleden[$j].", ";
					}
				}

				$songs .= "
						<div class='list-group'>
							<a href='' class='col-md-12 list-group-item '>	
								<div class='col-md-10'>
									<div class='row'>
										<p class='text-left'> Naam: ".$bandName."</p>
									</div>
									<div class='row'>
										<p class='text-left'> Leden: ".$leden."</p>
									</div>
									<div class='row'>
										<p class='text-left'> ".$bandBio."</p>
									</div> 
								</div>
							</a>
						</div>";
			}

			$this->set("message",$message);
			$this->set("filter","band");
			$this->set("title","Bands");
			$this->set("songs",$songs);
		}

		public function showArtists(){
			if (isset($_POST['searchQuery']) ){
				$artists = $this->_model->searchArtists($_POST['inputSearch']);
			}
			else{
				$artists = $this->_model->getArtists();
			}

			$message = "";
			if (count($artists) <=0) {
				$message = "<p class='bg-success'> Er zijn nog geen artiesten toegevoegd </p>";
			}


			$songs = "";
			for ($i=0; $i < count($artists); $i++) { 
				$artistName = $artists[$i]['Artist']['name'];
				$artistBirthdate = $artists[$i]['Artist']['birthdate'];
				$artistBio = $artists[$i]['Artist']['bio'];

				if($artistBirthdate == NULL){
					$artistBirthdate = "Geboortedatum onbekend";
				}
				if($artistBio == NULL){
					$artistBio = "Geen omschrijving beschikbaar";
				}

				$songs .= "
						<div class='list-group'>
							<a href='' class='col-md-12 list-group-item '>	
								<div class='col-md-10'>
									<div class='row'>
										<p class='text-left'> Naam: ".$artistName."</p>
									</div>
									<div class='row'>
										<p class='text-left'> Geboortedatum: ".$artistBirthdate."</p>
									</div> 
									<div class='row'>
										<p class='text-left'> ".$artistBio."</p>
									</div> 
								</div>
							</a>
						</div>";
			}

			$this->set("message",$message);
			$this->set("filter","artiesten");
			$this->set("title","Artiesten");
			$this->set("songs",$songs);
		}

		/*
		*	showSongs
		*	shows songs
		*/
		public function showSongs($genreID = null, $albumID = null){	
			$message = "";
			if( isset($_POST['searchQuery']) && isset($_POST['inputSearch']) ){
				$music = $this->_model->searchMusic($_POST['inputSearch'],$_SESSION['userID']);

				if(count($music) <= 0){
					$message = "<p class='bg-success'> Geen zoekresultaten </p>";
				}
					
			}else{
				$music = $this->_model->getMusic($_SESSION['userID']);
			}
			$artist = $this->_model->getArtists();
			$band = $this->_model->getBands();
			$genre = $this->_model->getGenres();
			$bandleden = $this->_model->getBandleden();
			$albums = $this->_model->getAlbums();
			$musiccontroller = new MusicController($music,$artist,$band,$bandleden,$genre,$albums);

			$title = "Mijn muziek";

			$songs = "";
			for($i = 0; $i < count($music); $i++){
				// Show only of given genre
				if ($genreID != null && $genreID != $musiccontroller->getGenreID($i)) {
					continue;
				}

				// Show only of given album
				if ($albumID != null){
					if($albumID == $musiccontroller->getAlbumID($i)){
						$title = $musiccontroller->getalbumName($i);
					}
					else{
						continue;
					}
				}

				$songName = $musiccontroller->getName($i);
				$songNameSrc =  str_replace(' ', '', $songName);
				$typeSong =  $musiccontroller->getTypeSong($i);
				$typeIMG =  $musiccontroller->getTypeIMG($i);
				$Genre = $musiccontroller->getGenre($i);
				$location = $musiccontroller->getLocation($i);
				$description = $musiccontroller->getDescription($i);
				$artist = $musiccontroller->getArtist($i);	

				// No artist = Band
				if($artist != null){
					$link = BASE_URL."gebruikers/update/artist/".$music[$i]['Music']['musicID'];

					$songSrc = BASE_URL."public/song/".$songNameSrc."-".str_replace(' ', '', $artist).".".$typeSong;
					if ($typeIMG != "") {
						$img = BASE_URL."public/img/".$songNameSrc."-".str_replace(' ', '', $artist).".".$typeIMG;
					}
					else{
						$img = BASE_URL."public/img/kriticalogo.png";
					}
					
				}
				else{
					$band = $musiccontroller->getBand($i);
					$bandleden = $musiccontroller->getBandleden($i);

					$link = BASE_URL."gebruikers/update/band/".$music[$i]['Music']['musicID']."/".count($bandleden);


					$artists = "";
					for ($j=0; $j < count($bandleden); $j++) { 
						if($j == count($bandleden)-1){ //if is last
							$artists .= $bandleden[$j]."";
						}
						else{
							$artists .= $bandleden[$j].", ";
						}
					}

					$songSrc = BASE_URL."public/song/".$songNameSrc."-".str_replace(' ', '', $band).".".$typeSong;
					if ($typeIMG != "") {
						$img = BASE_URL."public/img/".$songNameSrc."-".str_replace(' ', '', $band).".".$typeIMG;
					}
					else{
						$img = BASE_URL."public/img/kriticalogo.png";
					}
				}

				//sets typeSong for the audio player
				switch ($typeSong) {
					case 'mp3':
						$typeSong = "audio/mpeg";
						break;
					case 'Ogg':
						$typeSong = "audio/ogg";
						break;
					case 'Wav':
						$typeSong = "audio/wav";
						break;
				}

				$songs .= "
						<div class='list-group'>
							<a href='".$link."' class='col-md-12 list-group-item '>
								<div class='col-md-10'>
									<div class='row'>";
				if(isset($artist) ){
					$songs.= "<p class='text-left'> Naam: ".$songName." - ".$artist." </p>";
				}
				else if(isset($band)){
					$songs.= "<p class='text-left'> Naam: ".$songName." - ".$band." ( ".$artists." ) </p>";
				}
										 
				$songs .= "			</div>
									<div class='row'>
										<p class='text-left'> ".$description."</p>
									</div> ";

				if(isset($location)){
					$songs .= "
					<div class='row'>
						<p class='text-left'> Locatie: ".$location. " </p>
					</div>";
				}
				if(isset($Genre)){
					$songs .= "
					<div class='row'>
						<p class='text-left'> Genre: ".$Genre. " </p>
					</div>";
				}

				if($typeSong != ""){
					$songs .= "
					<div class='row'>
						<audio class='audio' preload='none' controls='controls'>
								<source src='".$songSrc."' type='".$typeSong."'>
							Your browser does not support the audio element.
						</audio>
					</div>";
				}

				$songs .= "		
								</div>
								<div class='col-md-2'>
									<img  class='img-thumbnail' src='".$img." '>
								</div>
							</a>
						</div>
				";
			}

			$this->set("title",$title);
			$this->set("songs",$songs);
			$this->set("message",$message);
		}

		public function addSong(){
			if(!$this->checkLogin()){
				header("Location:".BASE_URL."bezoekers/homepage");
				echo "Niet ingelogd";
				exit();
			}

			if (isset($_POST['preSubmit'])) {
				if ($_POST['inputType']){

					switch ($_POST['inputType']) {

						case 'artist':						
							$message =  "<p class='bg-success'> Artiest </p>";

							$artistInput = "
									<!-- Artist -->
									<div class='form-group'>
										<label for='inputArtist' class='col-sm-2 control-label'>Artiest:</label>
										<div class='col-sm-8'>
											<input type='text' class='form-control' id='inputArtist' name='inputArtist' placeholder='Artiest'>
										</div>
									</div>";
							$this->set("artistInput",$artistInput);
							break;

						case 'band':
							if($_POST['inputAmount'] != ""){
								if(is_numeric($_POST['inputAmount']) && $_POST['inputAmount'] > 0){
									$message =  "<p class='bg-success'> Band </p>";

									$bandInput = "
											<!-- Band -->
											<div class='form-group'>
												<label for='inputBand' class='col-sm-2 control-label'>Band:</label>
												<div class='col-sm-8'>
													<input type='text' class='form-control' id='inputBand' name='inputBand' placeholder='Band'>
												</div>
											</div>";

									for ($i=0; $i < $_POST['inputAmount']; $i++) { 
										$nummer = $i+1;
										$bandInput .= "
											<!-- Artist -->
											<div class='form-group'>
												<label for='inputArtist".$i."' class='col-sm-2 control-label'>Artiest ".$nummer.":</label>
												<div class='col-sm-8'>
													<input type='text' class='form-control' id='inputArtist".$i."'
																 name='inputArtist".$i."' placeholder='Artiest ".$nummer."'>
												</div>
											</div>";
									}
									$this->set("bandInput",$bandInput);
								}
								else{ //if $_POST['inputAmount'] is not a posative number
									$message = "<p class='bg-info'> Het aantal artiesten meer dan 0 zijn. </p>";
								}
							}
							else{ //if not set $_POST['inputAmount']
								$message = "<p class='bg-info'> Vul in om hoeveel artiesten het gaat. </p>";
							}
							break;				
						
						default:
							# code...
							break;
					}
				}
				else{ // if not set $_POST['inputType']
					$message = "<p class='bg-danger'> Kies of het om een artiest of band gaat. </p>";
				}
			}
			else{ // if not set $_POST['preSubmit']
				$message = "<p class='bg-info'> Kies of het nummer is gemaakt door een band of door 1 artiest </p>";
			}

			// if submit
			if(isset($_POST['submit'])){
				$genre = array( 'name' => $_POST['inputGenre'],
									'description' => '');

				// if set $_files song
				if ($_FILES['inputSong']['size'] != 0) {
					$typeSong = $_FILES['inputSong']['name'];
					$typeSong = explode('.', $typeSong);
					$typeSong = $typeSong[1];
				}
				else{
					$typeSong = NULL;
				}

				// if set $_files img
				if ($_FILES['inputIMG']['size'] != 0) {
					$typeIMG = $_FILES['inputIMG']['name'];
					$typeIMG = explode('.', $typeIMG);
					$typeIMG = $typeIMG[1];
				}
				else{
					$typeIMG = NULL;
				}


				// if Artist
				if($_POST['inputType'] == "artist"){

					$artist = array( 'name' => $_POST['inputArtist'],
									'bio' => '');

					$music = array( 'name' => $_POST['inputName'],
									'description' => $_POST['inputDescription'],
									'typeSong' => $typeSong,
									'typeIMG' => $typeIMG,
									'location' => $_POST['inputLocation'],
									'genre' => $genre,
									'artist' => $artist);

					//insert
					$this->_model->addMusicArtist($music,$_SESSION['userID']);

					// upload song
					if ($_FILES['inputSong']['size'] != 0) {
						$this->uploadSong(str_replace(' ', '',$music['name']) ,
											str_replace(' ', '',$music['artist']['name']));
					}
					// upload IMG
					if ($_FILES['inputIMG']['size'] != 0) {
						$this->uploadIMG(str_replace(' ', '',$music['name']) ,
										str_replace(' ', '',$music['artist']['name']));
					}



				}
				//if Band
				else if($_POST['inputType'] == "band"){
					

					$band = array( 	'name' => $_POST['inputBand'],
									'bio' => '');

					//puts all artist in array
					for ($i=0; $i < $_POST['inputAmount']; $i++) { 
						$inputArtist = "inputArtist".$i;
						$artists[$i]['name'] = $_POST[$inputArtist];
					}

					$music = array( 'name' => $_POST['inputName'],
									'description' => $_POST['inputDescription'],
									'typeSong' => $typeSong,
									'typeIMG' => $typeIMG,
									'location' => $_POST['inputLocation'],
									'genre' => $genre,
									'band' => $band,
									'artists' => $artists);
					//insert
					$this->_model->addMusicBand($music,$_SESSION['userID']);

					//upload song
					if ($_FILES['inputSong']['size'] != 0) {
						$this->uploadSong(str_replace(' ', '',$music['name']) ,
											str_replace(' ', '',$music['band']['name']));
					}
					// upload IMG
					if ($_FILES['inputIMG']['size'] != 0) {
						$this->uploadIMG(str_replace(' ', '',$music['name']) ,
										str_replace(' ', '',$music['band']['name']));
					}
				} // Band
				header("Location:".BASE_URL."gebruikers/homepage");
			} // if submit


			$this->set("message",$message);
		}

		public function uploadSong($songName,$artistName){
			$mime_type = array('audio/mp3', 'audio/ogg', 'audio/wav');
			if (in_array( $_FILES['inputSong']['type'], $mime_type))
			{
				$type = $_FILES['inputSong']['name'];
				$type = explode(".", $type);
				$dir = ROOT.DS.'public'.DS.'song'.DS;

				if ( !file_exists($dir)) {
					//makes directory
					mkdir($dir, 0777, true);
				}

				if(is_uploaded_file($_FILES['inputSong']['tmp_name'])){

					if(move_uploaded_file($_FILES['inputSong']['tmp_name'], $dir.$songName."-".$artistName.".".$type[1])){
						$message =  "<p class='bg-success'> Track succesvol geupload. </p>";
					}
					else{
						$message =  "<p class='bg-success'> Kon track niet uploaden. </p>";
					}
					
				}

			}
			else{
				echo "Verkeerde type song";
			}
		}

		public function uploadIMG($IMGName,$artistName){
			$mime_type = array('image/png', 'image/jpeg', 'image/pjpeg');
			if (in_array( $_FILES['inputIMG']['type'], $mime_type))
			{
				$type = $_FILES['inputIMG']['name'];
				$type = explode(".", $type);
				$dir = ROOT.DS.'public'.DS.'img'.DS;

				if ( !file_exists($dir)) {

					//makes directory
					mkdir($dir, 0777, true);
					mkdir($dir."thumbnails/", 0777, true);
				}

				if(is_uploaded_file($_FILES['inputIMG']['tmp_name'])){

					if(move_uploaded_file($_FILES['inputIMG']['tmp_name'], $dir.$IMGName."-".$artistName.".".$type[1])){
						$message =  "<p class='bg-success'> Afbeelding succesvol geupload. </p>";
					}
					else{
						$message =  "<p class='bg-success'> Kon afbeelding niet uploaden. </p>";
					}
					
				}

			}			
			else{
				echo "Verkeerde type IMG";
			}
		}
	}
?>