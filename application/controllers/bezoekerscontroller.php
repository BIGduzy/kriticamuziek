<?php
	class BezoekersController extends Controller
	{

		public function homepage(){

			$logo = BASE_URL."public/img/kriticalogo.png";
			$this->set("logo",$logo);

			$login = BASE_URL."bezoekers/login";
			$this->set("login",$login);

			$register = BASE_URL."bezoekers/register";
			$this->set("register",$register);


		}

		public function login(){
			$logo = BASE_URL."public/img/kriticalogo.png";
			$this->set("logo",$logo);

			$message = NULL;

			if(isset($_POST['submit'])){

				if(!empty($_POST['inputEmail']) && !empty($_POST['inputPassword'])){
					//get all users
					$users = $this->_model->getUsers($_POST);
					$userscontroller = new userscontroller($users);
					
					//checks if account exists
					if($userscontroller->checkUser($_POST['inputEmail'],$_POST['inputPassword'])){

						//checks if account is activated
						if($userscontroller->checkActivated($_POST['inputEmail'] )){
							$user = $userscontroller->getUserByEmail($_POST['inputEmail']);
							$_SESSION['userID'] =  $user['userID'];
							
							$message =  "<p class='bg-success'> U bent succesvol ingelogd </p>";
							header("Location:".BASE_URL."gebruikers/homepage");
						}
						else{
							$message = "<p class='bg-danger'> Uw account is nog niet geactiveerd. </p>";
						}
					}
					else{
						$message =  "<p class='bg-danger'> Gebruikersnaam of wachtwoord niet correct ingevoerd. </p>";
					}

				}
				else{
					$message =  "<p class='bg-danger'> Email-address of wachtwoord niet ingevuld. </p>";
				}
			}

			$this->set("message",$message);
		}

		public function register(){
			$logo = BASE_URL."public/img/kriticalogo.png";
			$this->set("logo",$logo);

			$message = NULL;

			//Check submit
			if(isset($_POST['submit'])){

				//checks if everything is filled in
				if(!empty($_POST['inputEmail']) && !empty($_POST['inputPassword1']) && !empty($_POST['inputPassword2'])
				 && !empty($_POST['inputName']) && !empty($_POST['inputDate'])){

					//password check
					if($_POST['inputPassword1'] == $_POST['inputPassword2'] ){

						//get all users
						$users = $this->_model->getUsers($_POST);
						$userscontroller = new userscontroller($users);

						//checks if email is in use
						if (!$userscontroller->checkEmail($_POST['inputEmail']) ) {
							$message = "<p class='bg-success'> Geregistreerd. </p>";
							$this->_model->addUser($_POST);
						}
						else{
							$message =  "<p class='bg-danger'> Het ingevoerde email-address is al in gebruik. </p>";
						}

					}
					else{
						$message =  "<p class='bg-warning'> De ingevoerde wachtwoorden komen niet overeen. </p>";
					}

				}
				else{
					$message =  "<p class='bg-warning'> &#201;&#233;n van de verplichte velden is niet ingevlud. </p>";
				}
			}

			$this->set("message",$message);
		}
	}
?>