<?php
/**
* 
*/
class AlbumController
{
	private $albums;

	function __construct($albumArray)
	{
		$this->albums = $albumArray;

		for ($i=0; $i < count($this->albums); $i++) { 
			$this->albums[$i] = new Album($this->albums[$i]['Album']);
		}
	}

	public function getName($id){
		for ($i=0; $i < count($this->albums); $i++) { 
			$albumID = $this->albums[$i]->getAlbumID();

			if($albumID == $id){
				return $this->albums[$i]->getName();
			}
		}
	}

	public function getalbumID($id){
		for ($i=0; $i < count($this->albums); $i++) { 
			$albumID = $this->albums[$i]->getAlbumID();

			if($albumID == $id){
				return $this->albums[$i]->getAlbumID();
			}
		}
	}
}

?>