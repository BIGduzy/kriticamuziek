<?php 
class ArtistController{
	private $artist;
	
	function __construct($artistArray){
		$this->artist = $artistArray;
		
		for ($i = 0; $i < count($this->artist); $i++) {
			$this->artist[$i] = new Artist($this->artist[$i]['Artist']);
		}
	}

	public function getName($id){
		for ($i=0; $i < count($this->artist); $i++) { 
			$artistID = $this->artist[$i]->getArtistID();

			if($artistID == $id){
				return $this->artist[$i]->getName();
			}
		}
	}

	public function getBirthdate($id){
		for ($i=0; $i < count($this->artist); $i++) { 
			$artistID = $this->artist[$i]->getArtistID();

			if($artistID == $id){
				return $this->artist[$i]->getBirthdate();
			}
		}
	}

	public function getBio($id){
		for ($i=0; $i < count($this->artist); $i++) { 
			$artistID = $this->artist[$i]->getArtistID();

			if($artistID == $id){
				return $this->artist[$i]->getBio();
			}
		}
	}
}
?>