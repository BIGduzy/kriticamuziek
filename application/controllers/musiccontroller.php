<?php
class MusicController{

	private $music;
	private $artists;
	
	function __construct($musicArray,$artistArray,$bandArray,$bandledenArray,$genreArray,$albumArray){
		$this->music = $musicArray;
		$this->artists = $artistArray;
		
		for ($i = 0; $i < count($this->music); $i++) {
			$this->music[$i] = new Music($this->music[$i]['Music']);
		}

		$this->artistController = new ArtistController($artistArray);
		$this->bandController = new BandController($bandArray,$bandledenArray);
		$this->genreController = new GenreController($genreArray);
		$this->albumController = new AlbumController($albumArray);
	}

	public function getName($id){
		return $this->music[$id]->getName();
	}

	public function getTypeSong($id){
		return $this->music[$id]->getTypeSong();
	}
		public function getTypeIMG($id){
		return $this->music[$id]->getTypeIMG();
	}

	public function getLocation($id){
		return $this->music[$id]->getLocation();
	}

	public function getDescription($id){
		return $this->music[$id]->getDescription();
	}

	public function getGenre($id){
		$genreID = $this->music[$id]->getGenre();
		if(isset($genreID)){
			$genre = $this->genreController->getName($genreID);
			return $genre;
		}
		
	}

	public function getGenreID($id){
		$genreID = $this->music[$id]->getGenre();
		if(isset($genreID)){
			$genre = $this->genreController->getGenreID($genreID);
			return $genre;
		}
	}

	public function getAlbumID($id){
		$albumID = $this->music[$id]->getAlbum();
		if(isset($albumID)){
			$albumID = $this->albumController->getAlbumID($albumID);
			return $albumID;
		}
	}

	public function getAlbumName($id){
		$albumID = $this->music[$id]->getAlbum();
		if(isset($albumID)){
			$albumName = $this->albumController->getName($albumID);
			return $albumName;
		}
	}

	public function getArtist($id){
		$artistID = $this->music[$id]->getArtist();
		if(isset($artistID)){
			$artist = $this->artistController->getName($artistID);
			return $artist;
		}
	}

	public function getBand($id){
		$bandID = $this->music[$id]->getBand();
		if (isset($bandID)) {
			$band = $this->bandController->getName($bandID);
			return $band;
		}
	}

	public function getBandleden($id){
		$bandID = $this->music[$id]->getBand();
		$bandleden = $this->bandController->getBandleden($bandID,$this->artists);

		return $bandleden;
	}

	public function getBandledenID($id){
		$bandID = $this->music[$id]->getBand();
		$bandleden = $this->bandController->getBandledenID($bandID,$this->artists);

		return $bandleden;
	}
	
}
?>