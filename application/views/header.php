<!DOCTYPE html>
<html>
	<head>
		<link rel="icon" href="<?php echo BASE_URL."public/img/kriticalogo.ico"; ?>">
		<title> Kritica Muziek</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel='stylesheet' href="<?php echo BASE_URL; ?>public/css/style.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>public/css/bootstrap.min.css" type="text/css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

	</head>
	<body>
		<div class="navbar navbar-inverse navbar-static-top">
			<div class="container">
				<div class="collapse navbar-collapse navHeaderCollapse pull-left">
					<ul class="nav navbar-nav navbar-right">
						<?php include("link.php"); ?>
					</ul>
				</div>
			</div>
		</div>

