<div class="container">
	<div class="row">
		<div class="col-md-2">
			<img  class="img-thumbnail" src="<?php echo $logo; ?> ">
		</div>
		<div class="col-md-10">
			<div class="page-header">
				<h1> Login </h1>
				<?php
					if(isset($message)){
						echo $message;
					} 
				?>
			</div>
			<div class="page-body">
				<!-- Form -->
				<form class="form-horizontal" role = "form" action="<?php echo BASE_URL."bezoekers/login" ?>" method="post">
					<!-- Email -->
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Email-address:</label>
						<div class="col-sm-8">
							<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email-address">
						</div>
					</div>
					<!-- Password1 -->
					<div class="form-group">
						<label for="inputPassword" class="col-sm-2 control-label">Wachtwoord:</label>
						<div class="col-sm-8">
							<input type="password" class="form-control" id="inputPassword1" name="inputPassword" placeholder="Wachtwoord">
						</div>
					</div>
					<!-- submit -->
					<div class="form-group">
						<div class="col-sm-8 col-sm-offset-2">
							<button type="submit" class="btn btn-default" name="submit"> login </button>
						</div>
					</div>
				</form>
			</div>

			</div>
		</div>
	</div>
</div>