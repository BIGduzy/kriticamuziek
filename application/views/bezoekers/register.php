<div class="container">
	<div class="row">
		<div class="col-md-2">
			<img  class="img-thumbnail" src="<?php echo $logo; ?>">
		</div>
		<div class="col-md-10">
			<div class="page-header">
				<h1> Register </h1>
				<?php
					if(isset($message)){
						echo $message;
					} 
				?>
			</div>
			<div class="page-body">
			
				<!-- Form -->
				<form class="form-horizontal" role = "form" action="<?php echo BASE_URL."bezoekers/register" ?>" method="post">
					<!-- Name -->
					<div class="form-group">
						<label for="inputName" class="col-sm-2 control-label">Naam:</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Naam">
						</div>
					</div>
					<!-- Birthdate-->
					<div class="form-group">
						<label for="inputDate" class="col-sm-2 control-label">Geboortedatum:</label>
						<div class="col-sm-8">
							<input type="date" class="form-control" id="inputDate" name="inputDate" placeholder="Naam">
						</div>
					</div>
					<!-- Email -->
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Email-address:</label>
						<div class="col-sm-8">
							<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email-address">
						</div>
					</div>
					<!-- Password1 -->
					<div class="form-group">
						<label for="inputPassword1" class="col-sm-2 control-label">Wachtwoord:</label>
						<div class="col-sm-8">
							<input type="password" class="form-control" id="inputPassword1" name="inputPassword1" placeholder="Wachtwoord">
						</div>
					</div>
					<!-- Password2 -->
					<div class="form-group">
						<label for="inputPassword2" class="col-sm-2 control-label">Herhaal wachtwoord:</label>
						<div class="col-sm-8">
							<input type="password" class="form-control" id="inputPassword2" name="inputPassword2" placeholder="Herhaal wachtwoord">
						</div>
					</div>
					<!-- submit -->
					<div class="form-group">
						<div class="col-sm-8 col-sm-offset-2">
							<button type="submit" class="btn btn-default" name="submit"> Registreer! </button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>