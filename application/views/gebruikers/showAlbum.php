<div class="container">
	<div class="row">

		<!-- search bar -->
		<?php include(ROOT.DS."application".DS."views".DS."gebruikers".DS."searchBar.php"); ?>
		
		<!-- content -->
		<div class="col-md-10">
			<div class="page-header">
				<h1> <?php echo $title ?> </h1>
				<?php
					if(isset($message)){
						echo $message;
					} 
				?>
			</div>
			<div class="page-body playlist">
				<?php 
					if(isset($songs)){
						echo $songs;
					}
				?>
			</div> <!-- Page body -->
		</div> <!-- col-md-10 -->

	</div> <!-- Row -->
</div> <!-- Container -->

<script type="text/javascript">
	var audio;
	var tracks;
	var current;

	play();

	audio[current].addEventListener('ended',function(e){
		current++;
		if (current > tracks) {
			//current = 0;
		}

		audio[current].pause();
    	audio[current].load();
		audio[current].play();
		console.log("current: " + current);
	});

	function play(){
		current = 0;
		audio = $('.audio');
		tracks = audio.length -1;
		console.log("tracks: " + tracks);
		console.log("current: " + current);
	}

</script>