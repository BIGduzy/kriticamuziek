		<div class="col-md-2">
		<img  class="img-thumbnail" src="<?php echo BASE_URL."public/img/kriticalogo.png"; ?> ">

		<form id='searchQuery' role = 'form' 
					action="<?php echo BASE_URL."gebruikers/homepage/".$filter.""; ?>"
					method='post'>
			<div class="input-group">
				<input type='text' class='form-control' id='inputSearch' name='inputSearch' placeholder='Zoeken...'>
				
				<span class='input-group-btn'>
					<button type='submit' class='btn btn-default' name='searchQuery'>
						<span class='glyphicon glyphicon-search'> </span>
					</button>	
				</span>			
			</div>
		</form>

		<!-- Links -->
		<ul class="nav nav-pills nav-stacked">
			<li ><a href="<?php echo BASE_URL."gebruikers/homepage/albums"; ?>" > Albums </a></li>
			<li ><a href="<?php echo BASE_URL."gebruikers/homepage/tracks"; ?>" > Tracks </a></li>
			<li ><a href="<?php echo BASE_URL."gebruikers/homepage/artiesten"; ?>" > Artiesten </a></li>
			<li ><a href="<?php echo BASE_URL."gebruikers/homepage/band"; ?>" > Bands </a></li>
			<li ><a href="<?php echo BASE_URL."gebruikers/homepage/genre"; ?>" > Genres </a></li>
		</ul>

		</div> <!-- col-md-2 -->