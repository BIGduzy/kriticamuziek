<div class="container">
	<div class="row">

		<!-- search bar -->
		<?php include(ROOT.DS."application".DS."views".DS."gebruikers".DS."searchBar.php"); ?>
		
		<!-- content -->
		<div class="col-md-10">
			<div class="page-header">
				<h1> <?php echo $title ?> </h1>
				<?php
					if(isset($message)){
						echo $message;
					} 
				?>
			</div>
			<div class="page-body">
					<?php 
						if(isset($songs)){
							echo $songs;
						}
					?>
				
			</div> <!-- Page body -->
		</div> <!-- col-md-10 -->

	</div> <!-- Row -->
</div> <!-- Container -->