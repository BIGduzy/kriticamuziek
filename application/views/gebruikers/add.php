<div class="container">
	<div class="row">
		<div class="col-md-2">
			<img  class="img-thumbnail" src="<?php echo $logo; ?> ">
		</div>

		<div class="col-md-10">
			<div class="page-header">
				<h1> Voeg een nummer toe! </h1>
				<?php
					if(isset($message)){
						echo $message;
					} 
				?>
			</div>
			<div class='page-body'>
			<?php
				if(isset($artistInput) || isset($bandInput)){ 
			?>
				<!-- Form -->
				<form id='add' class='form-horizontal' role = 'form' enctype="multipart/form-data" action="<?php echo BASE_URL."gebruikers/add" ?>" method='post'>
					<!-- Name -->
					<div class='form-group'>
						<label for='inputName' class='col-sm-2 control-label'>Song naam:</label>
						<div class='col-sm-8'>
							<input type='text' class='form-control' id='inputName' name='inputName' placeholder='Naam'>
						</div>
					</div>
					<?php
					if (isset($artistInput)) {
						echo $artistInput;
					}
					else if(isset($bandInput)){
						echo $bandInput;
					}
					?>
					<!-- Description-->
					<div class='form-group'>
						<label for='inputDescription' class='col-sm-2 control-label'>Omschrijving:</label>
						<div class='col-sm-8'>
							<input type='text' class='form-control' id='inputDescription' name='inputDescription' placeholder='Omschrijving'>
						</div>
					</div>
					<!-- Location -->
					<div class='form-group'>
						<label for='inputLocation' class='col-sm-2 control-label'>Locatie:</label>
						<div class='col-sm-8'>							
							<select id='inputLocation' name='inputLocation' class='form-control'>
								<option value="NAS">NAS</option>
								<option value="PC">PC</option>
								<option value="auto">auto</option>
								<option value="woonkamer">woonkamer</option>
								<option value="studeerkamer">studeerkamer</option>
							</select>
						</div>
					</div>
					<!-- Genre -->
					<div class='form-group'>
						<label for='inputGenre' class='col-sm-2 control-label'>Genre:</label>
						<div class='col-sm-8'>
							<input type='text' class='form-control' id='inputGenre' name='inputGenre' placeholder='Genre'>
						</div>
					</div>

					<!-- IMG -->
					<div class='form-group'>
				    	<label for='inputIMG' class='col-sm-2 control-label'>Afbeelding: </label>
				    	<div class='col-sm-8'>
    						<input type='file' id='inputIMG' name='inputIMG'>
						</div>
					</div>
					<!-- song -->
					<div class='form-group'>
				    		<label for='inputSong' class='col-sm-2 control-label'>Song:</label>
				    		<div class='col-sm-8'>
								<input type='file' id='inputSong' name='inputSong'>
							</div>
					</div>
					<input type="hidden" id="inputType" name="inputType" value="<?php echo $_POST['inputType']?>">
					<input type="hidden" id="inputAmount" name="inputAmount" value="<?php echo $_POST['inputAmount']?>">

					<!-- submit -->
					<div class='form-group'>
						<div class='col-sm-8 col-sm-offset-2'>
							<button type='submit' class='btn btn-default' name='submit'> Voeg toe! </button>
						</div>
					</div>
				</form>
				
					<?php
					}
					else{
					?>
				<!-- Form -->
				<form id='band' class='form-horizontal' role = 'form'
						 action="<?php echo BASE_URL."gebruikers/add" ?>" method='post'>

					<!-- Type -->
					<div class='form-group'>
						<label for='inputType' class='col-sm-2 control-label'>Artiest/Band:</label>
						<div class='col-sm-8'>
							<select name='inputType' id='inputType' class='form-control'>
								<option value="artist">Artiest</option>
								<option value="band">Band</option>
							</select>

						</div> <!-- col-sm-8 -->
					</div> <!-- Form-group -->

					<!-- Amount -->
					<div class='form-group'>
						<label for='inputAmount' class='col-sm-2 control-label'>Hoeveel Artiesten*:</label>
						<div class='col-sm-8'>
							<input type='number' class='form-control' id='inputAmount' name='inputAmount' 
									data-bv-integer="true" data-bv-integer-message="The value is not an integer" >
						</div> <!-- col-sm-8 -->
					</div> <!-- Form-group -->

					<!-- Submit -->
					<div class='form-group'>
						<div class='col-sm-8 col-sm-offset-2'>
							<button type='text' class='btn btn-default' name='preSubmit'> Voeg toe! </button>
						</div>
					</div>

					<!-- bonus text -->
					<div class='form-group'>
						* Dit velt kan leeg gelaten worden indien het <b>niet</b> om een band gaat.
					</div>
				</form>
			<?php
			}
			?>
			</div> <!-- page-body --> 
		</div> <!-- col-md-10 -->
	</div> <!-- row -->
</div> <!-- container -->