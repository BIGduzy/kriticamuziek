<div class="container">
	<div class="row">
		<div class="col-md-2">
			<img  class="img-thumbnail" src="<?php echo $logo; ?> ">
		</div>

		<div class="col-md-10">
			<div class="page-header">
				<?php
					if(isset($songName)){
						echo "<h1>".$songName."</h1>";
					}				
					if(isset($message)){
						echo $message;
					} 
				?>
			</div>
			<div class="page-body">
				<?php
				if ($showForm == 1) {
				?>
				<form id='update' class='form-horizontal' role = 'form' enctype="multipart/form-data"
							 action="<?php echo BASE_URL."gebruikers/update/".$type."/".$songID."/".$amount; ?>" method='post'>

								<!-- Name -->
								<div class='form-group'>
									<label for='inputName' class='col-sm-2 control-label'>Song naam:</label>
									<div class='col-sm-8'>
										<input type='text' class='form-control' id='inputName' 
												name='inputName' value='<?php echo $songName ?>' placeholder='<?php echo $songName ?>'>
									</div>
								</div>
								<?php
								if (isset($artistInput)) {
									echo $artistInput;
								}
								else if(isset($bandInput)){
									echo $bandInput;
								}
								?>
								<!-- Description-->
								<div class='form-group'>
									<label for='inputDescription' class='col-sm-2 control-label'>Omschrijving:</label>
									<div class='col-sm-8'>
										<input type='text' class='form-control' id='inputDescription'
												 name='inputDescription' value='<?php echo $description ?>' placeholder='<?php echo $description ?>'>
									</div>
								</div>
								<!-- Location -->
								<div class='form-group'>
									<label for='inputLocation' class='col-sm-2 control-label'>Locatie:</label>
									<div class='col-sm-8'>							
										<select id='inputLocation' name='inputLocation' class='form-control'>
											<option value="NAS"	<?php if ($location=="NAS") echo 'selected="selected"';?> >NAS</option>
											<option value="PC" <?php if ($location=="PC") echo 'selected="selected"';?> >PC</option>
											<option value="auto" <?php if ($location=="auto") echo 'selected="selected"';?> >auto</option>
											<option value="woonkamer" <?php if ($location=="woonkamer") echo 'selected="selected"';?> >woonkamer</option>
											<option value="studeerkamer" <?php if ($location=="studeerkamer") echo 'selected="selected"';?> >studeerkamer</option>
										</select>
									</div>
								</div>
								<!-- Genre -->
								<div class='form-group'>
									<label for='inputGenre' class='col-sm-2 control-label'>Genre:</label>
									<div class='col-sm-8'>
										<input type='text' class='form-control' id='inputGenre' name='inputGenre' value='<?php echo $genre ?>'>
									</div>
								</div>

								<!-- IMG -->
								<div class='form-group'>
							    	<label for='inputIMG' class='col-sm-2 control-label'>Afbeelding: </label>
							    	<div class='col-sm-2'>
							    		<?php 
							    			if (isset($IMG)) {
							    				echo $IMG;
							    			}
							    		?>
			    						<input type='file' id='inputIMG' name='inputIMG'>
									</div>
								</div>
								<!-- song -->
								<div class='form-group'>
							    		<label for='inputSong' class='col-sm-2 control-label'>Song:</label>
							    		<div class='col-sm-2'>
							    		<?php 
							    			if (isset($song)) {
							    				echo $song;
							    			}
							    		?>
											<input type='file' id='inputSong' name='inputSong'>
										</div>
								</div>

								<!-- submit -->
								<div class='form-group'>
									<div class='col-sm-8 col-sm-offset-2'>
										<button type='submit' class='btn btn-default' name='submit'> Wijzig! </button>
									</div>
										<a href='<?php echo BASE_URL."gebruikers/removeMusic/".$type."/".$songID; ?>' type="button" class="btn btn-danger">
											Verwijder
											<span class="glyphicon glyphicon-remove"></span>
										</a>
								</div>
							</form>						
						<?php
						} $showForm == 1;
						?>
			</div> <!-- Page body -->
		</div> <!-- col-md-10 -->

	</div> <!-- Row -->
</div> <!-- container -->