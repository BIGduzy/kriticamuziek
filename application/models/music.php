<?php
class Music{

	private $musicID;
	private $name;
	private $description;
	private $typeSong;
	private $typeIMG;
	private $location;
	private $genre;
	private $artist;
	private $band;
	private $album;

	public function __construct($music){
			$this->musicID = $music['musicID'];
			$this->name = $music['name'];
			$this->description = $music['description'];
			$this->typeSong = $music['typeSong'];
			$this->typeIMG = $music['typeIMG'];
			$this->location = $music['location'];
			$this->genre = $music['genre'];
			$this->artist = $music['artist'];
			$this->band = $music['band'];
			$this->album = $music['album'];
	}

	public function getMusicID() {
	    return $this->musicID;
	}

	public function setMusicID($musicID) {
	    $this->musicID = $musicID;
	    return $this;
	}

	public function getName() {
	    return $this->name;
	}

	public function setName($name) {
	    $this->name = $name;
	    return $this;
	}

	public function getDescription() {
	    return $this->description;
	}

	public function setDescription($description) {
	    $this->description = $description;
	    return $this;
	}

	public function getTypeSong() {
	    return $this->typeSong;
	}

	public function setTypeSong($typeSong) {
	    $this->typeSong = $typeSong;
	    return $this;
	}

	public function getTypeIMG() {
	    return $this->typeIMG;
	}

	public function setTypeIMG($typeIMG) {
	    $this->typeIMG = $typeIMG;
	    return $this;
	}

	public function getLocation() {
	    return $this->location;
	}

	public function setLocation($location) {
	    $this->location = $location;
	    return $this;
	}

	public function getGenre() {
	    return $this->genre;
	}

	public function setGenre($genre) {
	    $this->genre = $genre;
	    return $this;
	}

	public function getArtist() {
	    return $this->artist;
	}

	public function setArtist($artist) {
	    $this->artist = $artist;
	    return $this;
	}

	public function getBand() {
	    return $this->band;
	}

	public function setBand($band) {
	    $this->band = $band;
	    return $this;
	}

	public function getAlbum() {
	    return $this->album;
	}

	public function setAlbum($album) {
	    $this->album = $album;
	    return $this;
	}

}