<?php
class Album{
	private $albumID;
	private $name;
	private $description;

	public function __construct($album){
		$this->albumID = $album['albumID'];
		$this->name = $album['name'];
		$this->description = $album['description'];
	}

	public function getAlbumID() {
	    return $this->albumID;
	}

	public function setAlbumID($albumID) {
	    $this->albumID = $albumID;
	    return $this;
	}

	public function getName() {
	    return $this->name;
	}

	public function setName($name) {
	    $this->name = $name;
	    return $this;
	}

	public function getDescription() {
	    return $this->description;
	}

	public function setDescription($description) {
	    $this->description = $description;
	    return $this;
	}



}
?>