<?php
class Genre{
	private $genreID;
	private $name;
	private $description;

	public function __construct($genre){
		$this->genreID = $genre['genreID'];
		$this->name = $genre['name'];
		$this->description = $genre['description'];
	}

	public function getGenreID() {
	    return $this->genreID;
	}

	public function setGenreID($genreID) {
	    $this->genreID = $genreID;
	    return $this;
	}

	public function getName() {
	    return $this->name;
	}

	public function setName($name) {
	    $this->name = $name;
	    return $this;
	}

	public function getDescription() {
	    return $this->description;
	}

	public function setDescription($description) {
	    $this->description = $description;
	    return $this;
	}



}
?>