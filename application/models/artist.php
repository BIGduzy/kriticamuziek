<?php
class Artist{
	private $artistID;
	private $name;
	private $birthdate;
	private $bio;

	public function __construct($artist){	
			$this->artistID = $artist['artistID'];
			$this->name = $artist['name'];
			$this->birthdate = $artist['birthdate'];
			$this->bio = $artist['bio'];
	}

	public function getArtistID() {
	    return $this->artistID;
	}

	public function setArtistID($artistID) {
	    $this->artistID = $artistID;
	    return $this;
	}

	public function getName() {
	    return $this->name;
	}

	public function setName($name) {
	    $this->name = $name;
	    return $this;
	}

	public function getBirthdate() {
	    return $this->birthdate;
	}

	public function setBirthdate($birthdate) {
	    $this->birthdate = $birthdate;
	    return $this;
	}

	public function getBio() {
	    return $this->bio;
	}

	public function setBio($bio) {
	    $this->bio = $bio;
	    return $this;
	}


}
?>