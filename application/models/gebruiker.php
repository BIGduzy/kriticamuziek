<?php
	class Gebruiker extends model{

		public function getMusic($id){
			$query = "SELECT `music`.* FROM `user` , `music` , `ownedMusic` 
								WHERE `ownedMusic`.`user` = `user`.`userID` 
								AND `user`.`userID` = '{$id}'
								AND `ownedMusic`.`music` = `music`.`musicID`";
			// echo $query; exit();
			return $this->query($query);
		}

		public function searchMusic($inputSearch,$id){

			$query = "SELECT `music`.* FROM `user` , `music` , `ownedMusic` , `genre`, `artist`
								WHERE `ownedMusic`.`user` = `user`.`userID` 
								AND `user`.`userID` = '{$id}'
								AND `ownedMusic`.`music` = `music`.`musicID`
								AND (`music`.`name` LIKE '%{$inputSearch}%'
									 OR `music`.`location` LIKE '%{$inputSearch}%'
									 OR (`genre`.`name` LIKE '%{$inputSearch}%' AND `genre`.`genreID` = `music`.`genre`)
									 OR (`artist`.`name` LIKE '%{$inputSearch}%' AND `artist`.`artistID` = `music`.`artist`) )
								GROUP BY `music`.`musicID`";
			// echo $query; exit();
			return $this->query($query);
		}

		public function getMusicByMusicID($id){
			$query = "SELECT * FROM `music` 
								WHERE `musicID` = '{$id}'";
			return $this->query($query);
		}

		public function getAlbums(){
			$query = "SELECT `album`.* FROM `album`";			
			return $this->query($query);
		}

		public function searchAlbums($inputSearch){
			$query = "SELECT `album`.* FROM `album`
						WHERE `name` LIKE '%{$inputSearch}%'";
			return $this->query($query);
		}

		public function getArtists(){
			$query = "SELECT `artist`.* FROM `artist`";
			return $this->query($query);
		}

		public function searchArtists($inputSearch){
			$query = "SELECT `artist`.* FROM `artist`
						WHERE `name` LIKE '%{$inputSearch}%'";
			return $this->query($query);
		}

		public function getBands(){
			$query = "SELECT `band`.* FROM `band`";
			return $this->query($query);
		}

		public function searchBands($inputSearch){
			$query = "SELECT `band`.* FROM `band`
						WHERE `name` LIKE '%{$inputSearch}%'";
			return $this->query($query);
		}

		public function getBandleden(){
			$query = "SELECT `bandleden`.* FROM `bandleden`";
			return $this->query($query);
		}

		public function getGenres(){
			$query = "SELECT `genre`.* FROM `genre`";
			return $this->query($query);
		}

		public function searchGenres($inputSearch){
			$query = "SELECT `genre`.* FROM `genre`
						WHERE `name` LIKE '%{$inputSearch}%'";
			return $this->query($query);
		}

		public function addMusicBand($musicArray,$id){

			//Genre
			$query = "INSERT INTO `genre` (`genreID`,
 											`name`,
 											`description`)
								VALUES	( NULL,
										'{$musicArray['genre']['name']}',
										''); ";
			$this->query($query);

			//set genreID
			$query = "SET @genreID = LAST_INSERT_ID();";
			$this->query($query);

			// Band
			$query = "INSERT INTO `band` (`bandID`,
 											`name`,
 											`bio`)
								VALUES	( NULL,
										'{$musicArray['band']['name']}',
										''); ";
			$this->query($query);	

			//set bandID
			$query = "SET @bandID = LAST_INSERT_ID();";
			$this->query($query);

			for ($i=0; $i < count($musicArray['artists']); $i++) { 

				//Artist			
				$query = "INSERT INTO `artist` (`artistID`,
	 											`name`,
	 											`birthdate`,
	 											`bio`)
									VALUES	( NULL,
											'{$musicArray['artists'][$i]['name']}',
											Null,
											''); ";
				$this->query($query);

				//set artistID
				$query = "SET @artistID = LAST_INSERT_ID();";
				$this->query($query);

				//Bandleden
				$query = "INSERT INTO `bandLeden` (`bandLedenID`,
 											`artist`,
 											`band`)
								VALUES	( NULL,
										@artistID,
										@bandID );";
				$this->query($query);
				
			} // for

			//music
			$query = "INSERT INTO `music` (`musicID`,
 											`name`,
 											`description`,
 											`typeSong`,
 											`typeIMG`,
 											`location`,
 											`genre`,
 											`band`)
								VALUES	( NULL,
										'{$musicArray['name']}',
										'{$musicArray['description']}',
										'{$musicArray['typeSong']}',
										'{$musicArray['typeIMG']}',
										'{$musicArray['location']}',
										@genreID,
										@bandID); ";
			$this->query($query);

			//set musicID
			$query = "SET @musicID = LAST_INSERT_ID();";
			$this->query($query);

			//ownedMusic
			$query = "INSERT INTO `ownedMusic` (`ownedMusicID`,
 												 `user`,
 												 `music`)
								VALUES	( NULL,
										$id,
										@musicID); "; 
			// echo $query; // exit();
			$this->query($query);
		} // add music band

		public function addMusicArtist($musicArray,$id){

			//Genre
			$query = "INSERT INTO `genre` (`genreID`,
 											`name`,
 											`description`)
								VALUES	( NULL,
										'{$musicArray['genre']['name']}',
										''); ";
			$this->query($query);

			//set genreID
			$query = "SET @genreID = LAST_INSERT_ID();";
			$this->query($query);

			//Artist			
			$query = "INSERT INTO `artist` (`artistID`,
 											`name`,
 											`birthdate`,
 											`bio`)
								VALUES	( NULL,
										'{$musicArray['artist']['name']}',
										Null,
										''); ";
			$this->query($query);
			$query = "SET @artistID = LAST_INSERT_ID();";
			$this->query($query);

			//music
			$query = "INSERT INTO `music` (`musicID`,
 											`name`,
 											`description`,
 											`typeSong`,
 											`typeIMG`,
 											`location`,
 											`genre`,
 											`artist`)
								VALUES	( NULL,
										'{$musicArray['name']}',
										'{$musicArray['description']}',
										'{$musicArray['typeSong']}',
										'{$musicArray['typeIMG']}',
										'{$musicArray['location']}',
										@genreID,
										@artistID); ";
			$this->query($query);

			//set musicID
			$query = "SET @musicID = LAST_INSERT_ID();";
			$this->query($query);

			//ownedMusic
			$query = "INSERT INTO `ownedMusic` (`ownedMusicID`,
 												 `user`,
 												 `music`)
								VALUES	( NULL,
										$id,
										@musicID); "; 
			// echo $query; // exit();
			$this->query($query);
		} // add music artist

		public function updateMusicBand($musicArray){
			$query = "UPDATE `genre` 
								SET `name` = '{$musicArray['genre']['name']}'
								WHERE `genreID` = '{$musicArray['genre']['genreID']}' ";
			// echo $query; exit();
			$this->query($query);

			$query = "UPDATE `band` 
								SET `name` = '{$musicArray['band']['name']}'
								WHERE `bandID` = '{$musicArray['band']['bandID']}' ";
			// echo $query; exit();
			$this->query($query);

			for ($i=0; $i < count($musicArray['artists']); $i++) { 
				$query = "UPDATE `artist`
									SET `name` = '{$musicArray['artists'][$i]['name']}'
									WHERE `artistID` = '{$musicArray['artists'][$i]['artistID']}' ";
				// echo $query."<br>";
				$this->query($query);
			}

			$query = "UPDATE `music` 
								SET `name` = '{$musicArray['name']}',
									`description` = '{$musicArray['description']}',
									`typeSong` = '{$musicArray['typeSong']}',
									`typeIMG` = '{$musicArray['typeIMG']}',
									`location` = '{$musicArray['location']}'
								WHERE `musicID` = '{$musicArray['musicID']}' ";
			// echo $query; exit();
			$this->query($query);
		}

		public function updateMusicArtist($musicArray){
			// var_dump($musicArray);// exit();

			$query = "UPDATE `genre` 
								SET `name` = '{$musicArray['genre']['name']}'
								WHERE `genreID` = '{$musicArray['genre']['genreID']}' ";
			// echo $query; exit();
			$this->query($query);

			$query = "UPDATE `artist` 
								SET `name` = '{$musicArray['artist']['name']}'
								WHERE `artistID` = '{$musicArray['artist']['artistID']}' ";
			// echo $query; exit();
			$this->query($query);

			$query = "UPDATE `music` 
								SET `name` = '{$musicArray['name']}',
									`description` = '{$musicArray['description']}',
									`typeSong` = '{$musicArray['typeSong']}',
									`typeIMG` = '{$musicArray['typeIMG']}',
									`location` = '{$musicArray['location']}'
								WHERE `musicID` = '{$musicArray['musicID']}' ";
			// echo $query; exit();
			$this->query($query);								
		}

		public function removeMusicBand($musicArray,$ID){
			$query = "DELETE FROM `ownedMusic`
									WHERE `user` = '{$ID}'
									AND `music` = '{$musicArray['musicID']}' ";
			// echo $query."<br>";
			$this->query($query);

			$query = "DELETE FROM `music`
									WHERE `musicID` = '{$musicArray['musicID']}' ";
			// echo $query."<br>";
			$this->query($query);

			for ($i=0; $i < count($musicArray['artists']); $i++) { 
				$query = "DELETE FROM `bandLeden`
									WHERE `artist` = '{$musicArray['artists'][$i]['artistID']}'
									AND `band` =  {$musicArray['band']['bandID']}";
				// echo $query."<br>";
				$this->query($query);
			}
			for ($i=0; $i < count($musicArray['artists']); $i++) { 
				$query = "DELETE FROM `artist`
									WHERE `artistID` = '{$musicArray['artists'][$i]['artistID']}' ";
				// echo $query."<br>";
				$this->query($query);
			}

			$query = "DELETE FROM `band`
									WHERE `bandID` = '{$musicArray['band']['bandID']}' ";
			// echo $query."<br>";
			$this->query($query);	

			$query = "DELETE FROM `genre`
									WHERE `genreID` = '{$musicArray['genre']['genreID']}' ";
			// echo $query."<br>";
			$this->query($query);									
		}

		public function removeMusicArtist($musicArray,$ID){
			$query = "DELETE FROM `ownedMusic`
									WHERE `user` = '{$ID}'
									AND `music` = '{$musicArray['musicID']}' ";
			// echo $query."<br>";
			$this->query($query);

			$query = "DELETE FROM `music`
									WHERE `musicID` = '{$musicArray['musicID']}' ";
			// echo $query."<br>";
			$this->query($query);

			$query = "DELETE FROM `artist`
									WHERE `artistID` = '{$musicArray['artist']['artistID']}' ";
			// echo $query."<br>";
			$this->query($query);	

			$query = "DELETE FROM `genre`
									WHERE `genreID` = '{$musicArray['genre']['genreID']}' ";
			// echo $query."<br>";
			$this->query($query);									
		}


	}
?>