<?php
	/**
	* 
	*/
	class Band
	{
		private $bandID;
		private $name;
		private $bio;
		
		function __construct($band)
		{
			$this->bandID = $band['bandID'];
			$this->name = $band['name'];
			$this->bio = $band['bio'];
		}

		public function getBandID() {
		    return $this->bandID;
		}

		public function setBandID($bandID) {
		    $this->bandID = $bandID;
		    return $this;
		}

		public function getName() {
		    return $this->name;
		}

		public function setName($name) {
		    $this->name = $name;
		    return $this;
		}

		public function getBio() {
		    return $this->bio;
		}

		public function setBio($bio) {
		    $this->bio = $bio;
		    return $this;
		}

	}
?>