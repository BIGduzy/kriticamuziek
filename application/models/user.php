<?php
	class User
	{
		private $userID;
		private $name;
		private $birthdate;
		private $email;
		private $password;
		private $activated;
		
		function __construct($user)
		{
			$this->setUserID($user['userID']);
			$this->setName($user['name']);
			$this->setBirthdate($user['birthdate']);
			$this->setEmail($user['email']);
			$this->setPassword($user['password']);
			$this->setActivated($user['activated']);
		}

		public function getUserID() {
 		  	return $this->userID;
		}

		public function setUserID($userID) {
		    $this->userID = $userID;
		    return $this;
		}

		public function getName() {
		    return $this->name;
		}

		public function setName($name) {
		    $this->name = $name;
		    return $this;
		}

		public function getBirthdate() {
		    return $this->birthdate;
		}

		public function setBirthdate($birthdate) {
		    $this->birthdate = $birthdate;
		    return $this;
		}

		public function getEmail() {
		    return $this->email;
		}

		public function setEmail($email) {
		    $this->email = $email;
		    return $this;
		}

		public function getPassword() {
		    return $this->password;
		}

		public function setPassword($password) {
		    $this->password = $password;
		    return $this;
		}

		public function getActivated() {
		    return $this->activated;
		}

		public function setActivated($activated) {
		    $this->activated = $activated;
		    return $this;
		}


	}
?>